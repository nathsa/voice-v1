# Generated by Django 3.2.7 on 2021-12-12 18:24

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Crty_Id', models.CharField(max_length=100)),
                ('Crty_Name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Languages',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Lang_Id', models.CharField(max_length=100)),
                ('Lang_Name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='List',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('List_id', models.CharField(max_length=100)),
                ('Title', models.CharField(max_length=200)),
                ('Script_text', ckeditor.fields.RichTextField(blank=True, null=True)),
                ('User_id', models.CharField(max_length=100)),
                ('Char_count', models.CharField(max_length=100, null=True)),
                ('Language_id', models.CharField(max_length=100)),
                ('Voice_id', models.CharField(max_length=200)),
                ('Audio_file', models.CharField(max_length=100)),
                ('Notes', models.CharField(max_length=3000)),
                ('Country', models.CharField(max_length=100)),
                ('File_format', models.CharField(max_length=100)),
                ('Voice_effect', models.CharField(max_length=100)),
                ('Voice_token', models.CharField(max_length=100)),
                ('Status', models.CharField(max_length=200)),
                ('Voice_userid', models.CharField(max_length=100)),
                ('Voice_Price', models.CharField(max_length=100)),
                ('Accept_state', models.CharField(max_length=100)),
                ('admin_approved_state', models.CharField(max_length=200)),
                ('Delivery_date', models.CharField(max_length=100)),
                ('Delivered_on', models.CharField(max_length=100)),
                ('Created_at', models.CharField(max_length=100)),
                ('Updated_at', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Voice_Effect',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Vefft_Id', models.CharField(max_length=100)),
                ('Vefft_Name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Voice_Type',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Vtyp_Id', models.CharField(max_length=100)),
                ('Vtyp_Name', models.CharField(max_length=100)),
            ],
        ),
    ]
