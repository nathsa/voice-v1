from django.contrib import admin
from .models import List,Country,Languages,Voice_Type,Voice_Effect,User
# Register your models here.

@admin.register(User)
class ListAdmin5(admin.ModelAdmin):
 list_display = ['id','User_Id','Name', 'Email']

@admin.register(List)
class ListAdmin(admin.ModelAdmin):
 list_display = ['id','Title','Language_id', 'User_id']

@admin.register(Country)
class ListAdmin2(admin.ModelAdmin):
 list_display = ['id', 'Crty_Name']

@admin.register(Languages)
class ListAdmin3(admin.ModelAdmin):
 list_display = ['id', 'Lang_Name']

@admin.register(Voice_Type)
class ListAdmin4(admin.ModelAdmin):
 list_display = ['id', 'Vtyp_Name']

@admin.register(Voice_Effect)
class ListAdmin4(admin.ModelAdmin):
  list_display = ['id', 'Vefft_Name']