from django.db import models
from ckeditor.fields import RichTextField
class List(models.Model):
   List_id = models.CharField(max_length=100)
   Title= models.CharField(max_length=200)
   Script_text = RichTextField(blank=True, null=True)
   User_id = models.CharField(max_length=100)
   Char_count = models.CharField(max_length=100,null = True)
   Language_id = models.CharField(max_length=100)
   Voice_id = models.CharField(max_length=200)
   Audio_file = models.FileField(null=True)
   Notes = models.CharField(max_length=3000)
   Country = models.CharField(max_length=100)
   File_format = models.CharField(max_length=100)
   Voice_effect = models.CharField(max_length=100)
   Voice_token = models.CharField(max_length=100)
   Status = models.CharField(max_length=200)
   Voice_userid = models.CharField(max_length=100)
   Voice_Price = models.CharField(max_length=100)
   Accept_state = models.CharField(max_length=100)
   admin_approved_state = models.CharField(max_length=200)
   Delivery_date = models.CharField(max_length=100)
   Delivered_on = models.CharField(max_length=100)
   Created_at = models.CharField(max_length=100)
   Updated_at = models.CharField(max_length=100)


class Country(models.Model):
   Crty_Id = models.CharField(max_length=100)
   Crty_Name = models.CharField(max_length=100)

class Languages(models.Model):
   Lang_Id = models.CharField(max_length=100)
   Lang_Name = models.CharField(max_length=100)

class Voice_Type(models.Model):
   Vtyp_Id = models.CharField(max_length=100)
   Vtyp_Name = models.CharField(max_length=100)

class Voice_Effect(models.Model):
   Vefft_Id = models.CharField(max_length=100)
   Vefft_Name = models.CharField(max_length=100)


class User(models.Model):
   User_Id = models.CharField(max_length=100)
   Email = models.CharField(max_length=100)
   Name = models.CharField(max_length=100)
   Country = models.CharField(max_length=100)
   City = models.CharField(max_length=100)
   Password = models.CharField(max_length=3000)
   Facebook_id = models.CharField(max_length=100)
   google_id = models.CharField(max_length=100)
   Status = models.CharField(max_length=100)
   Account_type = models.CharField(max_length=100)
   User_type = models.CharField(max_length=100)
   Credits = models.CharField(max_length=100)
   Last_login = models.CharField(max_length=100)
   Created_at = models.DateTimeField(auto_now_add=True)
   Updated_at = models.CharField(max_length=100)
