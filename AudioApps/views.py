from django.shortcuts import render, redirect
from .models import List,Country,Languages,Voice_Type,Voice_Effect,User
from django.conf import settings
from Admin.models import Voice
from django.core.mail import send_mail
from django.contrib.auth import logout,login,authenticate
from django.contrib import messages,auth
from django.core.paginator import Paginator
from cryptography.fernet import Fernet
import base64

# Create your views here.
def home(request):
    if request.user.is_authenticated or request.session.get('name'):
         if request.session.get('name'):

             user = "welcome "+request.session['name']
         else:

             user = "welcome "+ request.user.username
         code=2
         return render(request, 'home.html', {'user': user,'code':code})
    else:
     code=1
     user='Please login...'
     return render(request, 'home.html',{'user':user,'code':code})

def filedownld(request,list_id):
    if request.user.is_authenticated or request.session.get('name'):
       Listdownlod = List.objects.get(List_id=list_id, admin_approved_state=1,Accept_state=1 )
       return render(request, 'userfiledownload.html',{'Listdownlod':Listdownlod})
    else:
        return render(request, 'logfst.html')

def voicelist(request):
    if request.user.is_authenticated or request.session.get('name'):
       voicelist = Voice.objects.all()
       paginator = Paginator(voicelist, 5)  # Show 25 contacts per page.
       page_number = request.GET.get('page')
       page_obj = paginator.get_page(page_number)
       return render(request, 'voicelist.html',{'page_obj':page_obj})
    else:
        return render(request, 'logfst.html')




def requestlist(request):
  if request.user.is_authenticated or request.session.get('name'):
     if  request.user.is_authenticated :
           user_id = request.user.username
     else:
          user_id = request.session.get('name')

     domain = request.get_host()
     print(domain)
     Listreq = List.objects.filter(User_id=user_id,Accept_state=1,admin_approved_state=1)
     paginator = Paginator(Listreq, 3)  # Show 25 contacts per page.
     page_number = request.GET.get('page')
     page_obj = paginator.get_page(page_number)
     return render(request, 'Requested.html',{'page_obj':page_obj,'domain':domain})
  else:
      return render(request, 'logfst.html')

def logout(request):
 if request.user.is_authenticated or request.session.get('name'):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('login')
    else:
        try:
            del request.session['name']
        except KeyError:
            pass
        return redirect('login')
 else:
    return render(request, 'logfst.html')

def RequestSubmit(request):
 if request.user.is_authenticated or request.session.get('name'):
    if request.session.get('name'):
       user_id= request.session.get('name')
    else:
        user_id = request.user.username

    if request.method == 'POST':
        title = request.POST.get('title')
        charCount = request.POST.get('charCount')
        req_id = str(request.POST.get('req_id'))
        content = str(request.POST.get('content'))
        language = str(request.POST.get('language'))
        voicetyp = str(request.POST.get('voicetyp'))
        contry = str(request.POST.get('contry'))
        voices = str(request.POST.get('voices'))
        Query = str(request.POST.get('Query'))

        voicesEfft = str(request.POST.get('voicesEfft'))

        voice_usrid = Voice.objects.get(vc_voicenm=voices)
        voiceid = User.objects.get(User_Id= voice_usrid.vc_UserId)


        List.objects.create(List_id=req_id, Title=title, Script_text=content, User_id=user_id, Char_count=charCount,
                            Language_id=language, Voice_id=voicetyp, Audio_file='', File_format='',
                            Voice_token='', Status=0, Voice_userid=voiceid.User_Id, Voice_Price='',
                            Accept_state=0, Notes=Query, Voice_effect=voicesEfft, Country=contry,
                            admin_approved_state=0, Delivery_date='', Delivered_on='',
                            Created_at='', Updated_at='')
        if Query:
            # Voice artist mail sending.....
            Email_voices = Voice.objects.get(vc_voicenm=voices)
            name = Email_voices.vc_voicenm
            subject = 'Mail sendings'
            message = 'Hi ' + name + ', '+ user_id +'  has requested your voice for ' + title + '. Please login to view the script.Please login to this URL..<url> '
            email = Email_voices.vc_email
            print(subject)
            print(message)
            print(email)
            send_mail(subject, message, settings.EMAIL_HOST_USER, [email], fail_silently=False)
            # Admin mail sending
            email = 'aadirishiinfotech@gmail.com'
            send_mail(subject, message, settings.EMAIL_HOST_USER, [email], fail_silently=False)


        Status = 'Pending'
        Conty = Country.objects.all()
        Lang = Languages.objects.all()
        VoiceEfft = Voice_Effect.objects.all()
        Voices = Voice.objects.all()
        Req_id = 0
        checkid1 = 0

        return render(request, 'Request.html',
                      {'Status': Status, 'checkid1': checkid1, 'Req_id': Req_id, 'conty': Conty, 'lang': Lang,
                       'VoiceEfft': VoiceEfft, 'Voices': Voices})

 else:
    return render(request, 'logfst.html')


def login(request):
 if request.user.is_authenticated or request.session.get('name'):
        session = request.session.get('name')
        return render(request, 'Login.html',{'session':session})
 else:
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        accntype = request.POST.get('accntype')


        if username == None and password == None :
            Status = 'Email or password is empty..'
            return render(request, 'Login.html', {'Status': Status})
        else:


            try:
                UserCheck = User.objects.get(Email=username,Account_type=accntype)
            except Exception as e:
                UserCheck = False
                Status = 'Wrong Credentials....'
                return render(request, 'Login.html', {'Status': Status})


            if UserCheck:
                fernet = Fernet(settings.ENCRYPT_KEY)
                passwordck = UserCheck.Password
                passwordck = base64.urlsafe_b64decode(passwordck)
                decrypass = fernet.decrypt(passwordck).decode('ascii')
                print(decrypass)
                if decrypass == password:

                    if accntype.upper() == 'USER':
                        request.session['name'] = UserCheck.User_Id
                        return redirect('homepage')
                    else:
                       request.session['artist'] = UserCheck.User_Id
                       return redirect('artisthome')
                       #return render(request, 'artisthome.html', {'user': user,'code':code})
                else:
                    Status = 'Wrong password......'
                    return render(request, 'Login.html', {'Status': Status})
            else:
                Status='Wrong Credentials....'
                return render(request, 'Login.html',{'Status':Status})
    else:
         return render(request, 'Login.html')



def UserRegister(request):

    if request.method == 'POST':
        user = request.POST.get('user')
        fullnm = str(request.POST.get('fullnm'))
        email = str(request.POST.get('email'))
        password = str(request.POST.get('password'))
        repassword = str(request.POST.get('repassword'))
        country = str(request.POST.get('country'))
        city = str(request.POST.get('city'))
        accntype = str(request.POST.get('accntype'))
        if password == repassword:

            try:
                emailexist = User.objects.filter(Email=email, Account_type__icontains=accntype)
            except Exception as e:
                emailexist = False
            if emailexist:
                Status = 'Email exists please try login...'
                return render(request, 'adminregister.html', {'Status': Status})
            else:

                fernet = Fernet(settings.ENCRYPT_KEY)

                encpassword = fernet.encrypt(password.encode('ascii'))
                encpassword = base64.urlsafe_b64encode(encpassword).decode("ascii")
                User.objects.create(User_Id=user,Email=email,Name=fullnm,Country=country,City=city,Password=encpassword,Facebook_id='',google_id='',
                   Status='Active',Account_type=accntype,User_type='',Credits=0,Last_login='',Created_at='',Updated_at='')
                Status = 'User Registered...'
                return render(request, 'Register.html', {'Status': Status})
        else:
                print('Password_missmatch')
                Status='Password Missmatch...'
                return render(request, 'Register.html',{'Status':Status})
    else:
      return render(request, 'Register.html')


def Request(request):

  if request.user.is_authenticated or request.session.get('name'):
       if request.session.get('name'):
          user_id = request.session.get('name')
       else:
           user_email = request.user.email
           print(user_email)
           userid = request.user.id

           try:
               Checkusr = User.objects.filter(User_Id__icontains=request.user.username)
           except Exception as e:
               print(e)
               User.objects.create(User_Id=request.user.username, Email=user_email, Name='', Country='', City='',
                                   Password='',
                                   Facebook_id='', google_id=userid,
                                   Status='Active', Account_type='', User_type='', Credits='', Last_login='',
                                   Created_at='', Updated_at='')

               Checkusr = False

           if Checkusr:
               user_id = request.user.username




       try:
         checkid = List.objects.get(User_id = user_id, Status=0)
       except Exception as e:
           template = "User doesn't exists...in list"
           message = template.format(type(e).__name__, e.args)
           checkid = False
       if  checkid:

           Status = 'Pending'
           Conty = Country.objects.all()
           Lang = Languages.objects.all()
           VoiceEfft = Voice_Effect.objects.all()
           Voices = Voice.objects.all()
           Req_id = 0
           checkid1 = checkid.Status

           return render(request, 'Request.html',{'Status':Status,'checkid1':checkid1,'Req_id':Req_id,'conty':Conty,'lang':Lang,'VoiceEfft':VoiceEfft,'Voices':Voices})
       else:
            try:
              Request_id = List.objects.filter().latest('id')
              Req_id = int(Request_id.List_id) + 1
            except Exception as e:
              template = "User doesn't exists...in list"
              message = template.format(type(e).__name__, e.args)
              Req_id = 1;
            print(Req_id)
            Conty = Country.objects.all()
            Lang = Languages.objects.all()
            VoiceEfft = Voice_Effect.objects.all()
            Voices = Voice.objects.all()
            Status= 'Active'
            checkid1 = 1
            return render(request, 'Request.html',{'Status':Status,'checkid1':checkid1,'Req_id':Req_id,'conty':Conty,'lang':Lang,'VoiceEfft':VoiceEfft,'Voices':Voices})
  else:
     return render(request, 'logfst.html')