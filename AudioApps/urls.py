from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.contrib import admin
#from django.contrib.auth import logout
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='homepage'),
    path('login/', views.login, name='login'),
    path('Register/', views.UserRegister, name='Register'),
    path('reqsubmit/', views.RequestSubmit, name='RequestSubmit'),
    path('accounts/profile/', views.Request, name='requestpg'),
    path('request/', views.Request, name='requestpg'),
    path('logout', views.logout, name='logout'),
    path('requestlist', views.requestlist, name='requestlist'),
    path('voicelist/', views.voicelist, name='voicelist'),
    path('filedownld/<str:list_id>/',views.filedownld,name='filedownld'),


   ]
if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)