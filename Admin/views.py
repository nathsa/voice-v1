from django.shortcuts import render, redirect
from AudioApps.models import User,List
from django.contrib import messages,auth
from django.conf import settings
from django.core.paginator import Paginator
from django.core.mail import send_mail
from django.core.files.storage import FileSystemStorage
from .models import Voice
from cryptography.fernet import Fernet
import base64
# Create your views here.




def voicelist(request):
     if  request.session.get('admin'):
          try:
            Listadmin = List.objects.filter(admin_approved_state=0)
          except Exception as e:
             Listadmin = False

          if Listadmin:
              paginator = Paginator(Listadmin, 3)  # Show 25 contacts per page.
              page_number = request.GET.get('page')
              page_obj = paginator.get_page(page_number)
              return render(request, 'adminlist.html',{'page_obj':page_obj})
          else:
              return render(request, 'adminlist.html')
     else:
         return render(request, 'adminlogfst.html')


def admndetail(request,list_id):
   if request.session.get('admin'):
          try:
            approver = List.objects.get(admin_approved_state=0, List_id=list_id)
          except Exception as e:
            approver = False

          if approver:
            return render(request, 'adminusrdt.html',{'approver':approver})
          else:
              return render(request, 'adminusrdt.html')
   else:
         return render(request, 'adminlogfst.html')

def adminusrlist(request):
   if request.session.get('admin'):
          try:
           Userlist = User.objects.all()
          except Exception as e:
            Userlist = False
          if  Userlist:
              paginator = Paginator(Userlist, 3)  # Show 25 contacts per page.
              page_number = request.GET.get('page')
              page_obj = paginator.get_page(page_number)
              return render(request, 'adminusrlist.html',{'page_obj':page_obj})
          else:
              return render(request, 'adminusrlist.html')
   else:
         return render(request, 'adminlogfst.html')

def admincreditlist(request):
   if request.session.get('admin'):
          try:
            Userlist = User.objects.all()
          except Exception as e:
            Userlist = False

          if   Userlist:
              paginator = Paginator(Userlist, 3)  # Show 25 contacts per page.
              page_number = request.GET.get('page')
              page_obj = paginator.get_page(page_number)
              return render(request, 'admincreditlist.html',{'page_obj':page_obj})
          else:
              return render(request, 'admincreditlist.html')
   else:
         return render(request, 'adminlogfst.html')

def adminapprove(request,list_id):
   if request.session.get('admin'):
          try:
           List.objects.filter(List_id=list_id).update(admin_approved_state=1)
          except Exception as e:
            pass

          return render(request, 'adminapprove.html')
   else:
         return render(request, 'adminlogfst.html')




def adminreject(request,list_id):
    if request.session.get('admin'):
      List.objects.filter(List_id=list_id).update(admin_approved_state=2,Status=2)
      # Voice artist mail sending.....
      emailuser = List.objects.get(List_id = list_id)

      if emailuser:
          Email_user = Voice.objects.get(vc_UserId=emailuser.Voice_userid)
          name = Email_user.vc_voicenm
          subject = 'Mail sendings'
          message = 'Hi ' + name + ', Admin has rejected your requests for ' + emailuser.Script_text + '. Please login to view the script.Please login to this URL..<url> '
          email = Email_user.vc_email
          print(subject)
          print(message)
          print(email)
          send_mail(subject, message, settings.EMAIL_HOST_USER, [email], fail_silently=False)
          # Admin mail sending
          email = 'aadirishiinfotech@gmail.com'
          send_mail(subject, message, settings.EMAIL_HOST_USER, [email], fail_silently=False)

    else:
        return render(request, 'adminlogfst.html')


def adminhome(request):
    if request.session.get('admin'):
        user = "welcome to admin " + request.session.get('admin')
        return render(request, 'adminhome.html',{'user':user})
    else:
        return render(request, 'adminlogfst.html')


def admincredit(request, user_id):
    if request.session.get('admin'):
        if request.method == 'POST':
             credit = request.POST.get('credit')
             userid = request.POST.get('userid')

             try:
                 credittest =User.objects.get(User_Id = userid)
             except Exception as e:
                 credittest = False

             if credittest:
                 oldcredit = credittest.Credits
                 newcredit = int(oldcredit) + int(credit)

                 User.objects.filter(User_Id = userid).update(Credits=newcredit)
                 status = 'Credit updated.....'
                 return render(request, 'adminaddcredit.html', {'status': status})
             else:
                 status="user doesn't exists....."
                 return render(request, 'adminaddcredit.html',{'status':status})

        else:
            Creditlist = User.objects.get(User_Id = user_id)
            return render(request, 'adminaddcredit.html',{'Creditlist':Creditlist})
    else:
        return render(request, 'adminlogfst.html')


def admnreg(request):
    if request.method == 'POST':
        user = request.POST.get('user')
        fullnm = request.POST.get('fullnm')
        email = request.POST.get('email')
        password = request.POST.get('password')
        repassword = request.POST.get('repassword')
        country = request.POST.get('country')
        city = request.POST.get('city')
        accntype = request.POST.get('accntype')


        if email == None and user == None and fullnm == None:
            Status = 'Please enter email,userid and full name'
            return render(request, 'adminregister.html', {'Status': Status})
        else:
            if password == repassword:

                try:
                    emailexist = User.objects.filter(Email=email,Account_type__icontains=accntype)
                except Exception as e:
                    emailexist = False
                if emailexist:
                    Status = 'Email exists please try login...'
                    return render(request, 'adminregister.html', {'Status': Status})
                else:
                    fernet = Fernet(settings.ENCRYPT_KEY)

                    encpassword = fernet.encrypt(password.encode('ascii'))
                    encpassword = base64.urlsafe_b64encode(encpassword).decode("ascii")

                    User.objects.create(User_Id=user, Email=email, Name=fullnm, Country=country, City=city,
                                        Password=encpassword, Facebook_id='', google_id='',
                                        Status='Active', Account_type=accntype, User_type='', Credits=0, Last_login='',
                                        Created_at='', Updated_at='')
                    Status = 'User Registered...'
                    return render(request, 'adminregister.html', {'Status': Status})
            else:
                print('Password_missmatch')
                Status = 'Password Missmatch...'
                return render(request, 'adminregister.html', {'Status': Status})
    else:
        return render(request, 'adminregister.html')


def admlogout(request):
     if  request.session.get('admin'):

               try:
                    del request.session['admin']
               except KeyError:
                    pass
               return redirect('homepage')
     else:
          return render(request, 'adminlogfst.html')


def admnlogin(request):
     if  request.session.get('admin'):
          user = "welcome to admin " + request.session.get('admin')
          return render(request, 'adminhome.html',{'user':user})
     else:
          if request.method == 'POST':
               username = request.POST.get('username')
               password = request.POST.get('password')

               try:
                 UserCheck = User.objects.get(User_Id=username,Account_type__icontains='admin')
               except Exception as e:
                 UserCheck = False
                 Status = 'Wrong Credentials....'
                 return render(request, 'Login.html', {'Status': Status})
               if UserCheck:
                    fernet = Fernet(settings.ENCRYPT_KEY)
                    passwordck = UserCheck.Password
                    passwordck = base64.urlsafe_b64decode(passwordck)
                    decrypass = fernet.decrypt(passwordck).decode('ascii')
                    print(decrypass)
                    if decrypass == password:
                        request.session['admin'] = UserCheck.User_Id
                        user = "welcome to admin " + request.session['admin']
                        return render(request, 'adminhome.html',{'user':user})
                    else:
                        Status = 'Wrong password......'
                        return render(request, 'adminlogin.html', {'Status': Status})
               else:
                    Status = 'Wrong Credentials....'
                    return render(request, 'adminlogin.html', {'Status': Status})

          else:
               return render(request, 'adminlogin.html')
