from django.contrib import admin
from .models import Voice


@admin.register(Voice)
class ImageAdmin(admin.ModelAdmin):
 list_display = ['id', 'vc_UserId','vc_voicenm','vc_voice', 'date']
