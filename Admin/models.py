from django.db import models

class Voice(models.Model):
 date = models.DateTimeField(auto_now_add=True)
 vc_listid= models.CharField(max_length=100)
 vc_UserId = models.CharField(max_length=100)
 vc_voicenm = models.CharField(max_length=100)
 vc_IsPremium = models.CharField(max_length=100)
 vc_Language_id = models.CharField(max_length=100)
 vc_Voice_type = models.CharField(max_length=100)
 vc_Sample_text = models.CharField(max_length=100)
 vc_human_voice = models.CharField(max_length=100)
 vc_gender = models.CharField(max_length=100)
 vc_email = models.CharField(max_length=200)
 vc_voice = models.FileField()