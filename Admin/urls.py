from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.contrib import admin
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('adminlist/', views.voicelist, name='adminlist'),
    path('admincreditlist/', views.admincreditlist, name='admincreditlist'),
    path('adminhome/', views.adminhome, name='adminhome'),
    path('adminlogin/', views.admnlogin, name='adminlogin'),
    path('adminreg/', views.admnreg, name='adminreg'),
    path('admincredit/<str:user_id>/', views.admincredit, name='admincredit'),
    path('admlogout', views.admlogout, name='admlogout'),
    path('adminusrlist', views.adminusrlist, name='adminusrlist'),
    path('admndetail/<str:list_id>', views.admndetail, name='admindtpg'),
    path('admnapprove/<str:list_id>', views.adminapprove, name='adminappvpg'),
    path('adminreject/<str:list_id>', views.adminreject, name='adminreject'),
   ]
if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)