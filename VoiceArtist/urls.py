from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.contrib import admin
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('artisthome/',views.artisthome,name='artisthome'),
    path('artistlist/',views.artistlist,name='artistlist'),
    path('artlogout/',views.artlogout,name='artlogout'),
    path('login/', views.login, name='artistlogin'),
    path('Register/', views.UserRegister, name='artistRegister'),
    path('voiceupld/<str:list_id>/',views.voiceupld,name='voiceupld'),
    path('voicereject/<str:list_id>/', views.voicereject, name='voicereject'),
    path('artistlistdt/<str:list_id>/',views.artistlistdt,name='artistlistdt'),
    path('artistaccept/<str:list_id>/',views.artistaccept,name='artistaccept'),
   ]
if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)