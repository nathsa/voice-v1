from django.shortcuts import render, redirect
from AudioApps.models import List,Country,Languages,Voice_Type,Voice_Effect,User
from django.core.paginator import Paginator
from django.db.models import Q, Count, Max
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from Admin.models import Voice
from django.http import HttpResponse
from django.core.mail import send_mail
from django.contrib.auth import logout,login,authenticate
from django.contrib import messages,auth
from cryptography.fernet import Fernet
import base64

def artisthome(request):
    if request.user.is_authenticated or request.session.get('artist'):
         if request.session.get('artist'):
             code = 1
             user = "welcome "+request.session['artist']
         else:

             user = "welcome "+ request.user.username
             code = 1
         return render(request, 'artisthome.html', {'user': user,'code':code})
    else:
     code= 0
     user='Please login...'
     return render(request, 'artisthome.html',{'user':user,'code':code})


def artistlist(request):
 if request.user.is_authenticated or request.session.get('artist'):

     Listadmin = List.objects.filter(Voice_userid=request.session.get('artist'))
     paginator = Paginator(Listadmin, 3)  # Show 25 contacts per page.
     page_number = request.GET.get('page')
     page_obj = paginator.get_page(page_number)
     return render(request, 'artistlist.html',{'page_obj':page_obj})
 else:
     return render(request, 'artistlogfst.html')

def artistlistdt(request,list_id):
    if request.session.get('artist'):

            if request.method == 'POST':
                req_id = request.POST.get('req_id')
                price = str(request.POST.get('price'))
                Delirydt = str(request.POST.get('Delirydt'))

                List.objects.filter(List_id=list_id).update(Voice_Price=price,Delivery_date=Delirydt,Accept_state=1)
                return render(request, 'artistaccept.html')
            else:
                try:
                    listartist = List.objects.get(Voice_userid=request.session.get('artist'), admin_approved_state=1,
                                               List_id=list_id, Accept_state=1)
                except Exception as e:
                    template = "User doesn't exists...after admin approve.."
                    message = template.format(type(e).__name__, e.args)
                    print(message)
                    listartist= False

                if listartist:
                    checkid1 = listartist.Accept_state
                    return render(request, 'artistlistdt.html', {'checkid1': checkid1, 'approver': listartist})
                else:
                    try:
                      listart = List.objects.get(Voice_userid=request.session.get('artist'),admin_approved_state=0, List_id=list_id,Accept_state=1)
                    except Exception as e:
                       template = "User doesn't exists...in list"
                       message = template.format(type(e).__name__, e.args)
                       listart = False
                    if  listart:
                        checkid1 = listart.Accept_state
                        return render(request, 'artistlistdt.html', {'checkid1': checkid1,'approver': listart})
                    else:
                        try:
                            checkid = List.objects.get(List_id=list_id,Voice_userid=request.session.get('artist'),admin_approved_state=0,Accept_state=0)
                        except Exception as e:
                            template = "User doesn't exists...in list"
                            message = template.format(type(e).__name__, e.args)
                            print(message)
                            checkid = False
                        if checkid:
                            checkid1 = checkid.Accept_state
                            try:
                                listart = List.objects.get(admin_approved_state=0, List_id=list_id)
                            except Exception as e:
                                template = "User doesn't exists...in block list"
                                message = template.format(type(e).__name__, e.args)
                            return render(request, 'artistlistdt.html', {'checkid1': checkid1, 'approver': listart})
                        else:
                            msg = 'This request has been rejected by admin'
                            return HttpResponse(msg, content_type='text/plain')


    else:
        return render(request, 'artistlogfst.html')


def voiceupld(request,list_id):
    if request.session.get('artist'):

        if request.method == 'POST':
            req_id = request.POST.get('req_id')
            voicenm = str(request.POST.get('voicenm'))
            myfile = request.FILES['myfile']
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            uploaded_file_url = fs.url(filename)
            List.objects.filter(List_id=list_id).update(Audio_file=uploaded_file_url,Status=1)
            return render(request, 'artistaccept.html')
        else:
            try:
                query = Q(List_id=list_id)
                query.add(Q(Voice_userid=request.session.get('artist')), Q.AND)
                query.add(Q(Status=0), Q.AND)
                query.add(Q(Accept_state=1), Q.AND)
                query.add(Q(admin_approved_state=1), Q.AND)
                checkid = List.objects.get(query)
            except Exception as e:
                template = "User doesn't exists...in upload"
                message = template.format(type(e).__name__, e.args)
                print(message)
                checkid = False
            if checkid:
                checkid1= checkid.admin_approved_state
                return render(request, 'artistvoiceupld.html', {'list_id': list_id,'checkid1':checkid1})
            else:
                checkid1 = 0
                return render(request, 'artistvoiceupld.html',{'list_id':list_id,'checkid1':checkid1})
    else:
        return render(request, 'artistlogfst.html')


def voicereject(request,list_id):
    if request.session.get('artist'):
      List.objects.filter(List_id=list_id).update(Accept_state=2, Status=2)
      # Voice artist mail sending.....
      emailuser = List.objects.get(List_id = list_id)

      if emailuser:
          Email_user = User.objects.get(User_Id=emailuser.User_id)
          name = Email_user.Name
          subject = 'Mail sendings'
          message = 'Hi ' + name + ', '+emailuser.Voice_userid +'has rejected your voice for ' + emailuser.Script_text + '. Please login to view the script.Please login to this URL..<url> '
          email = Email_user.Email
          print(subject)
          print(message)
          print(email)
          send_mail(subject, message, settings.EMAIL_HOST_USER, [email], fail_silently=False)
          # Admin mail sending
          email = 'aadirishiinfotech@gmail.com'
          send_mail(subject, message, settings.EMAIL_HOST_USER, [email], fail_silently=False)
          msg = 'This request is rejected'

          return HttpResponse(msg, content_type='text/plain')
    else:
        return render(request, 'artistlogfst.html')


def artlogout(request):
    if request.session.get('artist'):

        try:
            del request.session['artist']
        except KeyError:
            pass
        return redirect('artisthome')
    else:
        return render(request, 'artistlogfst.html')


def artistaccept(request,list_id):
    if request.session.get('artist'):

        try:
           List.objects.filter(List_id=list_id).update(Accept_state=1)
        except KeyError:
            pass
        return render(request, 'artistaccept.html')
    else:
        return render(request, 'artistlogfst.html')


def login(request):
 if request.user.is_authenticated or request.session.get('artist'):
        session = request.session.get('artist')
        return render(request, 'artistlogin.html',{'session':session})
 else:
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        accntype = request.POST.get('accntype')
        try:
            UserCheck = User.objects.get(Email=username,Account_type=accntype)
        except Exception as e:
            Status = 'Wrong Credentials....'
            return render(request, 'artistlogin.html', {'Status': Status})

        if UserCheck:
            fernet = Fernet(settings.ENCRYPT_KEY)
            passwordck = UserCheck.Password
            passwordck = base64.urlsafe_b64decode(passwordck)
            decrypass = fernet.decrypt(passwordck).decode('ascii')
            print(decrypass)
            if decrypass == password:
                if accntype.upper() == 'USER':
                    request.session['name'] = UserCheck.User_Id
                    return redirect('homepage')
                else:
                   request.session['artist'] = UserCheck.User_Id
                   return redirect('artisthome')
                   #return render(request, 'artisthome.html', {'user': user,'code':code})
            else:
                Status = 'Password missmatch....'
                return render(request, 'artistlogin.html', {'Status': Status})
        else:
            Status='Wrong Credentials....'
            return render(request, 'artistlogin.html',{'Status':Status})
    else:
         return render(request, 'artistlogin.html')



def UserRegister(request):

    if request.method == 'POST':
        user = request.POST.get('user')
        fullnm = request.POST.get('fullnm')
        email = request.POST.get('email')
        password = request.POST.get('password')
        repassword = request.POST.get('repassword')
        country = request.POST.get('country')
        city = request.POST.get('city')
        accntype = request.POST.get('accntype')
        print(user)
        print(fullnm)
        print(email)
        print(password)


        if email == None and user == None and fullnm == None:
            Status = 'Please enter email,userid and full name'
            return render(request, 'artistregister.html', {'Status': Status})
        else:
            if password == repassword :
                try:
                    emailexist = User.objects.filter(Email=email,Account_type__icontains=accntype)
                except Exception as e:
                    emailexist = False
                if emailexist:
                    Status = 'Email exists please try login...'
                    return render(request, 'artistregister.html', {'Status': Status})
                else:
                    fernet = Fernet(settings.ENCRYPT_KEY)

                    encpassword = fernet.encrypt(password.encode('ascii'))
                    encpassword = base64.urlsafe_b64encode(encpassword).decode("ascii")
                    User.objects.create(User_Id=user,Email=email,Name=fullnm,Country=country,City=city,Password=encpassword,Facebook_id='',google_id='',
                       Status='Active',Account_type=accntype,User_type='',Credits=0,Last_login='',Created_at='',Updated_at='')
                    Status = 'User Registered...'
                    return render(request, 'artistregister.html', {'Status': Status})
            else:
                    print('Password_missmatch')
                    Status='Password Missmatch...'
                    return render(request, 'artistregister.html',{'Status':Status})
    else:
      return render(request, 'artistregister.html')
